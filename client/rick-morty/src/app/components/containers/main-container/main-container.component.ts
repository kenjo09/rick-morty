import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { CharacterService } from 'src/app/services/character/character.service';
import { EpisodeService } from 'src/app/services/episode/episode.service';
import { LocationService } from 'src/app/services/location/location.service';

@Component({
  selector: 'app-main-container',
  templateUrl: './main-container.component.html',
  styleUrls: ['./main-container.component.scss']
})
export class MainContainerComponent implements OnInit {

  @Input() inputData: any;
  // character
  characterCardView = false;
  characterCardDetail = false;
  characterDetails: any;
  characterData: any;

  // location
  locationCardView = false;
  locationDetails: any;

  // episode
  episodeCardView = false;
  episodeDetails: any;
  episodeList: any;
 
  // subGroupCharacter
  subGroupCharacter = false;
  isCollapsed = false;
  isCollapsedSub = false;
  subLocationShow = false;
  subEpisodeShow = false;

  selectedIndex: any;
  constructor(
    private characterService: CharacterService,
    private locationService: LocationService,
    private episodeService: EpisodeService
  ) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.inputData = changes.inputData.currentValue;
    this.reset();

    if (this.inputData['type'] == 'character') {
      this.characterCardView = true;
    }

    if (this.inputData['type'] == 'location') {
      this.locationCardView = true;
    }

    if (this.inputData['type'] == 'episode') {
      this.episodeCardView = true;
    }
  }
  
  viewCharacterDetails(character: any, i?: any) {
    if (i != undefined) {
      this.selectedIndex = i;
    }

    this.characterCardDetail = true;
    this.characterDetails = character;
    this.isCollapsed = true;

    const params = {
      episode: JSON.stringify(character.episode)
    };

    this.episodeService.getEpisode(params).subscribe(
      response => {
        this.episodeList = response;
      }
    );
  }

  characterLocation(location: any) {
    const params = {
      location: JSON.stringify(location.id)
    };

    this.locationService.getLocation(params).subscribe(
      response => {
        this.viewLocationDetails(response[0]);
      }
    );
  }

  viewLocationDetails(location: any, i?: any) {
    if (i != undefined) {
      this.selectedIndex = i;
    }
    
    this.characterCardDetail = false;
    this.locationDetails = location;
    this.subLocationShow = true;
    this.subEpisodeShow = false;
    this.subGroupCharacter = true;
    const params = {
      character: JSON.stringify(location.residents)
    };

    this.characterService.getCharacter(params).subscribe(
      response => {
        this.characterData = response;
        this.isCollapsed = true;
      });
  }

  viewEpisodeDetails(episode: any, i?: any) {
    if (i != undefined) {
      this.selectedIndex = i;
    }
    
    this.characterCardDetail = false;
    this.episodeDetails = episode;
    this.subEpisodeShow = true;
    this.subLocationShow = false;
    this.subGroupCharacter = true;
    const params = {
      character: JSON.stringify(episode.characters)
    };

    this.characterService.getCharacter(params).subscribe(
      response => {
        this.characterData = response;
        this.isCollapsed = true;
      });
  }

  back() {
    this.selectedIndex = [];
    this.characterDetails = [];
    this.characterCardDetail = false;
  }

  prev(prev: any) {
    const params = {
      page: prev
    };
    this.selectedIndex = '';
    this.callServiceApi(params);
  }

  next(next: any) {
    const params = {
      page: next
    };
    this.selectedIndex = '';
    this.callServiceApi(params);
  }

  callServiceApi(params: any) {
    switch (this.inputData['type']) {
      case 'character':
        this.characterService.getCharacter(params).subscribe(
          response => {
            response['type'] = 'character';
            this.inputData = response;
          });
        break;
      case 'location':
        this.locationService.getLocation(params).subscribe(
          response => {
            response['type'] = 'location';
            this.inputData = response;
          });
        break;
      case 'episode':
        this.episodeService.getEpisode(params).subscribe(
          response => {
            response['type'] = 'episode';
            this.inputData = response;
          });
        break;
    }
  }

  reset() {
    this.characterCardView = false;
    this.characterCardDetail = false;
    this.characterDetails = [];
    this.characterData = [];
    this.isCollapsed = false;

    this.locationCardView = false;
    this.locationDetails = [];

    this.episodeCardView = false;
    this.episodeDetails = [];

    this.isCollapsedSub = false;
    this.subLocationShow = false;
    this.subGroupCharacter = false;
    this.selectedIndex = [];
  }
}
