import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { CharacterService } from 'src/app/services/character/character.service';

@Component({
  selector: 'app-character-button',
  templateUrl: './character-button.component.html',
  styleUrls: ['./character-button.component.scss']
})
export class CharacterButtonComponent implements OnInit {
  @Output() characterData = new EventEmitter<any>();
  constructor(private characterService: CharacterService) { }

  ngOnInit(): void {
  }

  getCharacter() {
    this.characterService.getCharacter().subscribe(
      response => {
        response['type'] = 'character';
        this.characterData.emit(response);
    });
  }
}
