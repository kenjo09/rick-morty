import { Component, OnInit, EventEmitter, Output  } from '@angular/core';
import { LocationService } from 'src/app/services/location/location.service';

@Component({
  selector: 'app-location-button',
  templateUrl: './location-button.component.html',
  styleUrls: ['./location-button.component.scss']
})
export class LocationButtonComponent implements OnInit {
  @Output() locationData = new EventEmitter<any>();
  constructor(private locationService: LocationService) { }

  ngOnInit(): void {
  }

  getLocation() {
    this.locationService.getLocation().subscribe(
      response => {
        response['type'] = 'location';
        this.locationData.emit(response);
    });
  }
}
