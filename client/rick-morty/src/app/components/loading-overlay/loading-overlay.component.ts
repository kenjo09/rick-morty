import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { LoadingOverlayService } from 'src/app/services/loading-overlay/loading-overlay.service';

@Component({
  selector: 'app-loading-overlay',
  templateUrl: './loading-overlay.component.html',
  styleUrls: ['./loading-overlay.component.scss']
})
export class LoadingOverlayComponent implements OnInit {

  showLoadingOverlay = false;
  constructor(private loadingOverlayService: LoadingOverlayService, private cdRef:ChangeDetectorRef) { }

  ngOnInit(): void {
    this.init();
  }

  init() {

    this.loadingOverlayService.getSpinnerObserver().subscribe((status) => {
      this.showLoadingOverlay = (status === 'start');
      this.cdRef.detectChanges();
    });
  }

}
