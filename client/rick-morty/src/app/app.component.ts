import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'rick-morty';
  data = {
    'info': [],
    'results' : [],
    'type': ''
  };

  setData(event: any) {
    this.data = event;
  }
}


