import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EpisodeService {
  private apiUrl = 'http://local.rick-morty.com/episodes/api';

  constructor(private http: HttpClient) { }

  getEpisode(filter?: any) {
    return this.http.get<any>(this.apiUrl,{params: filter});
  }
}
