import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CharacterService {
  private apiUrl = 'http://local.rick-morty.com/characters/api';

  constructor(private http: HttpClient) { }

  getCharacter(filter?: any) {
    return this.http.get<any>(this.apiUrl,{params: filter});
  }
}

