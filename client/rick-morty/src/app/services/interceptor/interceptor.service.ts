import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LoadingOverlayService } from '../loading-overlay/loading-overlay.service';
import { finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  constructor(private loadingOverlayService: LoadingOverlayService) { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.loadingOverlayService.requestStarted();

    return next.handle(req).pipe(
      finalize(
        ()=> {
          this.loadingOverlayService.requestEnded();
        }
      )      
    );
  }
}
