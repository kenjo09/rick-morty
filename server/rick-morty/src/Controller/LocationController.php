<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcherInterface;

use App\Services\LocationService;
/**
 * @Rest\Route("/locations")
 */
class LocationController extends AbstractController
{

    /**
     * @Rest\Get("/api")
     * @QueryParam(name="page")
     * @QueryParam(name="location")
     */
    public function getLocation(
        LocationService $locationService,
        ParamFetcherInterface $paramFetcher
    ) : JsonResponse {
        $params = $paramFetcher->all();
        $response = $locationService->getLocationData($params);

        return new JsonResponse($response);
    }
}