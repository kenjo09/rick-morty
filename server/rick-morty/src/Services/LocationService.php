<?php

namespace App\Services;
use App\Services\RickMortyApiService;

class LocationService extends RickMortyApiService {
    const LOCATION_ENDPOINT = 'location/';

    public function getLocationData(array $params = []) : array {
        $filter = $this->convertToQueryParams($params);
        $response = $this->getData(self::LOCATION_ENDPOINT, $filter);

        if (empty($response)) {
            return [];
        }

        $arrResponse = $response;
        $pageFlag = false;

        if (empty($params['location'])) {
            $arrResponse = $response['results'];
            $pageFlag = true;
        }

        foreach($arrResponse as $key => $val) {
            $arrResponse[$key]['residentCounts'] = count($val['residents']);
            $charId = [];
            foreach($val['residents'] as $k) {
                $charId[] = substr($k, strpos($k, "/character/") + 11);
            }
            $arrResponse[$key]['residents'] = $charId;
        }

        if ($pageFlag) {
            if (!empty($response['info']['next'])) {
                $response['info']['next'] = $this->getPage($response['info']['next']);
            }
    
            if (!empty($response['info']['prev'])) {
                $response['info']['prev'] = $this->getPage($response['info']['prev']);
            }

            return $response;
        }

        return $arrResponse;
    }
}